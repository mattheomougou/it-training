<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formation informatique : ib - groupe Cegos</title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="accueil.css">
    <link rel="stylesheet" href="header.css">
</head>
<body>
<header>
<nav class="custom_nav1 navbar bg-light ">
    <ul class="nav justify-content-end">
        <li class="nav-item custom_nav_item1">
            <a class="nav-link" aria-current="page" href="#"><img class="custom_img_nav1" src="image/contact.png" alt="Contact">Contact</a>
        </li>
        <li class="nav-item custom_nav_item2">
            <a class="nav-link"  href="#"><img class="custom_img_nav" src="image/espace_client.png" alt="Espace Client">Espace Client</a>
        </li>
        <li class="nav-item custom_nav_item3">
            <a class="nav-link" href="#"><img class="custom_img_nav" src="image/panier.png" alt="Panier">Panier</a>
        </li>
    </ul></nav>

<nav class="navbar navbar-expand-lg navbar-light bg-light custom_nav">
    <div class="container-fluid">
        <a class="navbar-brand" href="accueil.jsp"><img class="custom_img" src="image/téléchargement.png"></a> <h6 class="custom_h6_nav">Formation aux technologies et métiers du Numérique</h6>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Formation
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="formation.jsp">Formation</a></li>
                        <li><a class="dropdown-item" href="#">Formations informatiques</a></li>
                        <li><a class="dropdown-item" href="#">Formations métiers</a></li>
                        <li><a class="dropdown-item" href="#">Formations bureautiques</a></li>
                        <li><a class="dropdown-item" href="#">Formations par éditeurs</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Certification
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Solution
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Financement
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2 form-control-custom" type="search" placeholder="Thème, référence, ..." aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>

</nav>
<hr class="hr_nav">
</header>

<main>
    <section class="premiere_section">
            <label class="label_premiere_section" for="recherche_premiere_section">Trouver une formation à distance ou en présentiel</label>
        <form class="d-flex">
            <input class="form-control me-2 formulaire_premiere_section form-control-custom" id="recherche_premiere_section" type="search" placeholder="Thème, référence, mot clé,..." aria-label="Search">
            <button class="btn btn-danger" type="submit">Search</button>
        </form>
    </section>
    <section class="deuxieme_section">
        <a href="">
        <img class="deuxieme_section_img" src="image/aws.png" alt="aws">
            <div class="texte_deuxieme_section">
            <h2 class="deuxieme_section_h2">Actualité</h2>
            <h3>ib devient AWS Training Partner</h3><br>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mam nulla, obcaecati officia quos ratione recusandae soluta ut veniam, voluptatem? Consequatur cupiditate eligendi eos labore magnam molestias nesciunt voluptas?
            </p>
            </div>
        </a>
    </section>

    <section class="troisieme_section">
        <p class="troisieme_section_paragraphe">
            Solutions
        </p>
        <article class="troisieme_section_premier_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-fne.png" alt="...">

        </article>
        <article class="troisieme_section_deuxieme_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-distance.png" alt="...">

        </article>
        <article class="troisieme_section_troisieme_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-intra.png" alt="...">

        </article>
    </section>
    <section class="troisieme_section">
        <p class="troisieme_section_paragraphe">
            Financements
        </p>
        <article class="troisieme_section_premier_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-mixte.png" alt="...">

        </article>
        <article class="troisieme_section_deuxieme_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-poe.png" alt="...">

        </article>
        <article class="troisieme_section_troisieme_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-reclassement.png" alt="...">

        </article>
        <article class="troisieme_section_quatrieme_article">
            <h4>Toutes nos formation</h4>
            <p>Retrouvez notre offre de formations dédiées aux technologies et métiers du Numérique...</p>
            <img src="image/mea-catalogue.png" alt="...">

        </article>
    </section>
    <section class="quatrieme_section">
        <p class="troisieme_section_paragraphe">
            Actualités
        </p>
        <article class="quatrieme_section_premier_article">
            <h5>CPF: une nouvelle charte de déontologie pour les organismes de formation</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid doloribus earum error esse et in inventore minus necessitatibus, nobis quibusdam!</p>
        </article>
    </section>

</main>
<footer class="w-100 py-4 flex-shrink-0 custom-flex-grow footer-custom">
        <div class="container py-4">
            <div class="row gy-4 gx-5">
                <div class="col-lg-4 col-md-6">
                    <h5 class="h1 text-white">IT-Training</h5>
                    <p class="small text-muted">L'apprentissage n'est que le souvenir de ce qui ne faut pas faire, n'ayez pas peur de l'échec.</p>
                    <p class="small text-muted mb-0">&copy; Copyrights. All rights reserved. <a class="text-primary" href="#">ittraining.fr</a></p>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">A propos d'IT-Training</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">Nous connaître</a></li>
                        <li><a href="#">Group Cegos</a></li>
                        <li><a href="#">Actualité</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Aide</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Nous contacter</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Suivez-nous</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="https://www.linkedin.com">Linkedin</a></li>
                        <li><a href="https://www.twitter.com">Twitter</a></li>
                        <li><a href="https://www.facebook.com">Facebook</a></li>
                        <li><a href="https://www.youtube.fr">Youtube</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>