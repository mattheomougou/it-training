<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="formation.css">
<link rel="stylesheet" href="footer.css">
</head>
<body>
	<header>
<nav class="custom_nav1 navbar bg-light ">
    <ul class="nav justify-content-end">
        <li class="nav-item custom_nav_item1">
            <a class="nav-link" aria-current="page" href="#"><img class="custom_img_nav1" src="image/contact.png" alt="Contact">Contact</a>
        </li>
        <li class="nav-item custom_nav_item2">
            <a class="nav-link"  href="#"><img class="custom_img_nav" src="image/espace_client.png" alt="Espace Client">Espace Client</a>
        </li>
        <li class="nav-item custom_nav_item3">
            <a class="nav-link" href="#"><img class="custom_img_nav" src="image/panier.png" alt="Panier">Panier</a>
        </li>
    </ul></nav>

<nav class="navbar navbar-expand-lg navbar-light bg-light custom_nav">
    <div class="container-fluid">
        <a class="navbar-brand" href="accueil.jsp"><img class="custom_img" src="image/téléchargement.png"></a> <h6 class="custom_h6_nav">Formation aux technologies et métiers du Numérique</h6>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Formation
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Gouvernance information</a></li>
                        <li><a class="dropdown-item" href="#">Formations informatiques</a></li>
                        <li><a class="dropdown-item" href="#">Formations métiers</a></li>
                        <li><a class="dropdown-item" href="#">Formations bureautiques</a></li>
                        <li><a class="dropdown-item" href="#">Formations par éditeurs</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Certification
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Solution
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Financement
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2 form-control-custom" type="search" placeholder="Thème, référence, ..." aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>

</nav>
<hr class="hr_nav">
</header>
	<div>
		<h1>Notre catalogue de formations</h1>
	</div>
	
	<div class="container">
		<div class="row align-items-start">
			<div class="col">
<div class="list-group">
		<button type="button"
			class="list-group-item list-group-item-action active"
			aria-current="true">Notre catalogue de Formations</button>
		<button type="button" class="list-group-item list-group-item-action ">A
			second item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			third button item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			fourth button item</button>
		<button type="button" class="list-group-item list-group-item-action"
			>A disabled button item</button>
			<button type="button" class="list-group-item list-group-item-action">A
			fourth button item</button>
	</div>
	
</div>
	<div class="col">
			<div class="list-group">
		<button type="button"
			class="list-group-item list-group-item-action active"
			aria-current="true">Notre catalogue de Formations</button>
		<button type="button" class="list-group-item list-group-item-action ">A
			second item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			third button item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			fourth button item</button>
		<button type="button" class="list-group-item list-group-item-action"
			>A disabled button item</button>
	</div>
	
	</div>
			
			<div class="col">
<div class="list-group">
		<button type="button"
			class="list-group-item list-group-item-action active"
			aria-current="true">Notre catalogue de Formations</button>
		<button type="button" class="list-group-item list-group-item-action ">A
			second item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			third button item</button>
		<button type="button" class="list-group-item list-group-item-action">A
			fourth button item</button>
		<button type="button" class="list-group-item list-group-item-action">A 
		disabled button item</button>
	</div>

		</div>
	</div>
</div>

<footer class="w-100 py-4 flex-shrink-0 custom-flex-grow footer-custom">
        <div class="container py-4">
            <div class="row gy-4 gx-5">
                <div class="col-lg-4 col-md-6">
                    <h5 class="h1 text-white">IT-Training</h5>
                    <p class="small text-muted">L'apprentissage n'est que le souvenir de ce qui ne faut pas faire, n'ayez pas peur de l'échec.</p>
                    <p class="small text-muted mb-0">&copy; Copyrights. All rights reserved. <a class="text-primary" href="#">ittraining.fr</a></p>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">A propos d'IT-Training</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">Nous connaître</a></li>
                        <li><a href="#">Group Cegos</a></li>
                        <li><a href="#">Actualité</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Aide</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Nous contacter</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Suivez-nous</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="https://www.linkedin.com">Linkedin</a></li>
                        <li><a href="https://www.twitter.com">Twitter</a></li>
                        <li><a href="https://www.facebook.com">Facebook</a></li>
                        <li><a href="https://www.youtube.fr">Youtube</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>