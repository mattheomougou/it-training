package fr.ibcegos.papotecar.modele.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.papotecar.modele.dao.UtilisateurDAO;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;

public class UtilisateurManager {
	
	// cl� : champs test� - valeur : message
	Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String nom, String email, String mdp, String confirmMdp) {
		verifNom(nom);
		verifEmail(email);
		verifMdp(mdp, confirmMdp);
		return erreurs;
	}
	
	public void verifNom(String nom) {
		if(nom.length() <= 3) { // to1, to exclu - tot1, toto, t0to accept�
			erreurs.put("nom", "Le nom doit faire au moins 4 caract�res");
			return;
		}
		for(int i=0; i<3; i++) {
			char caractere = nom.toUpperCase().charAt(i);
			// majuscule ACSII 65 � 90
			if(!((int) caractere >= 65 && (int) caractere <= 90)) { // si je ne suis PAS entre 65 et 90
				erreurs.put("nom", "Le nom doit commencer par 3 lettres");
				// tot1, toto, tot0 accept� - t0to exclu
			}
		}
	}
	
	public void verifEmail(String email) {
		if(!(email.trim().endsWith(".fr") || email.endsWith(".com") || email.endsWith(".bzh"))) {
			erreurs.put("email", "L'email doit terminer par .bzh, .com ou .fr");
		}
	}
	
	public void verifMdp(String mdp, String confirmMdp) {
		if(!(mdp.length() > 3)) {
			erreurs.put("mdp", "Les mot de passe doit faire au moins 4 caract�res");
		}
		if(!mdp.equals(confirmMdp)) {
			erreurs.put("confirmMdp", "Les mots de passes ne sont pas identiques");
		}
	}

	public void enregistrer(Utilisateur utilisateur) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		utilisateurDAO.enregistrer(utilisateur);
	}

	public Utilisateur findByEmailAndMdp(String email, String mdp) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		Utilisateur utilisateur = utilisateurDAO.findByEmailAndMdp(email, mdp);
		return utilisateur;
	}

	public List<Utilisateur> findAll() {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		return utilisateurDAO.findAll();
	}
	
}
