package fr.ibcegos.papotecar.modele.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class PoolConnexion {
	
	public static Connection getConnexion() {
		Connection connexion = null;
		try {
			 // charger le driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connexion = 
					DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databasename=BDD_PROJET_FIL_ROUGE", "sa", "Pa$$w0rd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connexion;
	}

}
