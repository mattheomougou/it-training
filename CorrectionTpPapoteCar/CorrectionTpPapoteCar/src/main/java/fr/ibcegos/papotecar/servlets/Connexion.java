package fr.ibcegos.papotecar.servlets;

import java.io.IOException;

import fr.ibcegos.papotecar.modele.managers.UtilisateurManager;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/connexion")
public class Connexion extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// récupération des paramètres
		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		
		// appel au manager
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		Utilisateur utilisateur = utilisateurManager.findByEmailAndMdp(email, mdp);
		
		// redirection en fonction du résultat
		
		if(utilisateur == null) {
			request.setAttribute("message", "Email ou mot de passe invalide");
			this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp")
			.forward(request, response);
		} else {
			// on stocke l'attribut dans la session
			request.getSession().setAttribute("utilisateur", utilisateur);
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
			.forward(request, response);
		}
		
	}

}
