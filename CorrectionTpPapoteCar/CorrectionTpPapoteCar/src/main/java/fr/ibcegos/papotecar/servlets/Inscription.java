package fr.ibcegos.papotecar.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import fr.ibcegos.papotecar.modele.managers.UtilisateurManager;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;

@WebServlet("/inscription")
public class Inscription extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration de donn�es
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String motDePasse = request.getParameter("motDePasse");
		String confirmMotDePasse = request.getParameter("confirmMotDePasse");
		String pseudo = request.getParameter("pseudo");
		String adresse = request.getParameter("adresse");
		String ville = request.getParameter("ville");
		String entreprise = request.getParameter("entreprise");
		
		// encapsulation de la verification dans un manager
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		
		Map<String,String> erreurs = utilisateurManager.verif(nom, mail, motDePasse, confirmMotDePasse);
		
		// redirection
		if(erreurs.isEmpty()) {
			// si pas d'erreur alors on enregistre en BDD
			Utilisateur utilisateur = new Utilisateur(nom,prenom, mail, motDePasse,pseudo,adresse,ville,entreprise);
			utilisateurManager.enregistrer(utilisateur);
			request.setAttribute("messageSucces", "Vous �tes bien inscrit");
			this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp")
			.forward(request, response);
		} else {
			request.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp")
			.forward(request, response);
		}

	}

}
