package fr.ibcegos.papotecar.modele.managers;

import fr.ibcegos.papotecar.modele.dao.TrajetDAO;
import fr.ibcegos.papotecar.modele.objets.Trajet;

public class TrajetManager {

	public void enregistrer(Trajet trajet) {
		TrajetDAO trajetDAO = new TrajetDAO();
		trajetDAO.enregistrer(trajet);
	}

}
