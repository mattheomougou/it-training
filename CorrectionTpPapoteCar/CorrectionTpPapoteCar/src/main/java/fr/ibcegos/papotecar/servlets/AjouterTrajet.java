package fr.ibcegos.papotecar.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

import fr.ibcegos.papotecar.modele.managers.TrajetManager;
import fr.ibcegos.papotecar.modele.objets.Trajet;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;

@WebServlet("/ajouterTrajet")
public class AjouterTrajet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouterTrajet.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String description = request.getParameter("description");
		String villeDepart = request.getParameter("villeDepart");
		String villeArrivee = request.getParameter("villeArrivee");
		LocalDateTime dateHeureDepart = 
				LocalDateTime.parse(request.getParameter("dateHeureDepart"));
		double prix = Double.valueOf(request.getParameter("prix"));
		int nombreDePlace = Integer.valueOf(request.getParameter("nombrePlace"));
		
		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
		
		Trajet trajet = new Trajet(description, villeDepart, villeArrivee, dateHeureDepart,
				prix, nombreDePlace, utilisateurConnecte);
		
		TrajetManager trajetManager = new TrajetManager();
		trajetManager.enregistrer(trajet);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
		.forward(request, response);
		
	}

}
