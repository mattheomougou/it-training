package fr.ibcegos.papotecar.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import fr.ibcegos.papotecar.modele.objets.Trajet;

public class TrajetDAO {

	public void enregistrer(Trajet trajet) {
		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "INSERT INTO Trajet(description, villeDepart, villeArrivee, "
				+ "dateHeureDepart, prix, nombreDePlace, conducteur) "
				+ "VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setString(1, trajet.getDescription());
			pstmt.setString(2, trajet.getVilleDepart());
			pstmt.setString(3, trajet.getVilleArrivee());
			pstmt.setTimestamp(4, Timestamp.valueOf(trajet.getDateHeureDepart()));
			pstmt.setDouble(5, trajet.getPrix());
			pstmt.setInt(6, trajet.getNombreDePlace());
			pstmt.setInt(7, trajet.getConducteur().getId());
			pstmt.executeUpdate();
			cnx.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
