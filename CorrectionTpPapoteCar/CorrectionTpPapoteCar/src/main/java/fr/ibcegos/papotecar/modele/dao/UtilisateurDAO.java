package fr.ibcegos.papotecar.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.papotecar.modele.objets.Utilisateur;

public class UtilisateurDAO {
	
	public void enregistrer(Utilisateur utilisateur) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			// 3 - Ex�cuter la requete
			String query = "INSERT INTO Stagiaire(nom,prenom,mail, pseudo ,mot_de_passe, date_inscription, adresse,ville,entreprise) "
					+ "VALUES(?,?,?,?,CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(? as varchar)),2),?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, utilisateur.getNom());
			// le nom de l'utilisateur est mis en tant que 
			// string dans le 1er parametre de la query
			pstmt.setString(2, utilisateur.getPrenom());
			pstmt.setString(3, utilisateur.getMail());
			pstmt.setString(4, utilisateur.getPseudo());
			pstmt.setString(5, utilisateur.getMotDePasse());
			pstmt.setDate(6, Date.valueOf(utilisateur.getDateInscription()));
			pstmt.setString(7, utilisateur.getAdresse());
			pstmt.setString(8, utilisateur.getVille());
			pstmt.setString(9, utilisateur.getEntreprise());
			
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Utilisateur findByEmailAndMdp(String mail, String mot_de_passe) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Stagiaire WHERE mail = ? and "
					+ "mot_de_passe = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2,mot_de_passe);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	public void updateCompteurAndDateDeBlocage(String email) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "UPDATE Utilisateur SET compteurBlocageCompte = compteurBlocageCompte + 1"
					+ " WHERE email = ? and compteurBlocageCompte < 5";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			pstmt.executeUpdate();
			
			query = "SELECT * from UTILISATEUR where email = ?";
			pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();
			int compteurBlocageCompte = 0;
			while(rs.next()) {
				compteurBlocageCompte = rs.getInt("compteurBlocageCompte");
			}
			if(compteurBlocageCompte == 5) {
				query = "UPDATE Utilisateur SET dateFinBlocage = ? where email = ?";
				pstmt = connexion.prepareStatement(query);
				pstmt.setDate(1, Date.valueOf(LocalDate.now().plusDays(1)));
				pstmt.setString(2, email);
				pstmt.executeUpdate();
			}
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void resetCompteurAndDateDeBlocage(String email) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "UPDATE Utilisateur SET compteurBlocageCompte = 0, dateFinBlocage = null WHERE email = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			// 3 - Ex�cuter la requete
			pstmt.executeUpdate();
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Utilisateur> findAll() {
		
		List<Utilisateur> utilisateurs = new ArrayList<>();
		
		// 1 - R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "SELECT * FROM Utilisateur";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Utilisateur utilisateur = remplirUtilisateur(rs);
				utilisateurs.add(utilisateur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return utilisateurs;
	}
	
	private Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setPrenom(rs.getString("prenom"));
		utilisateur.setMail(rs.getString("mail"));
		utilisateur.setPseudo(rs.getString("pseudo"));
		utilisateur.setMotDePasse(rs.getString("mot_de_passe")); 
		utilisateur.setDateInscription(rs.getDate("date_inscription").toLocalDate());					
		utilisateur.setAdresse(rs.getString("adresse"));
		utilisateur.setVille(rs.getString("ville"));
		utilisateur.setEntreprise(rs.getString("entreprise"));
		return utilisateur;
	}


}
