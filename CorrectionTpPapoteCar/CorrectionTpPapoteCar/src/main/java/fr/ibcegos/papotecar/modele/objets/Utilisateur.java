package fr.ibcegos.papotecar.modele.objets;

import java.time.LocalDate;


public class Utilisateur {
	
	private int id; // car on enregistre un utilisateur en BDD
	private String nom;
	private String prenom;
	private String mail;
	private String motDePasse;
	private LocalDate dateInscription;
	private String pseudo;
	private String adresse;
	private String ville;
	private String entreprise;
	
	
	public Utilisateur() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Utilisateur(String nom, String prenom, String mail, String motDePasse,
			String pseudo, String adresse, String ville, String entreprise) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.motDePasse = motDePasse;
		this.dateInscription = LocalDate.now();
		this.pseudo = pseudo;
		this.adresse = adresse;
		this.ville = ville;
		this.entreprise = entreprise;
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", motDePasse="
				+ motDePasse + ", dateInscription=" + dateInscription + ", pseudo=" + pseudo + ", adresse=" + adresse
				+ ", ville=" + ville + ", entreprise=" + entreprise + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getMotDePasse() {
		return motDePasse;
	}


	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}


	public LocalDate getDateInscription() {
		return dateInscription;
	}


	public void setDateInscription(LocalDate dateInscription) {
		this.dateInscription = dateInscription;
	}


	public String getPseudo() {
		return pseudo;
	}


	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public String getEntreprise() {
		return entreprise;
	}


	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}




}
