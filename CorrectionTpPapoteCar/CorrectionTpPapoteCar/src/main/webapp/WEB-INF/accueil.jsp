<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>
</head>
<body>

<%-- <jsp:include page="header.jspf"></jsp:include> --%>
<%@ include file="header.jspf" %>

<h1>Bienvenue sur IT-Training ! Le site de formation malin !</h1>

<c:if test="${sessionScope.utilisateur != null}">
	<p>Vous �tes connect� en tant que ${sessionScope.utilisateur.nom}</p>
</c:if>

<c:if test="${sessionScope.utilisateur == null}">
	<p>Vous n'�tes pas connect�</p>
</c:if>


</body>
</html>